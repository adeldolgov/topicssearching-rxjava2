package adeldolgov.tinkoff.asynclecture.data

import adeldolgov.tinkoff.asynclecture.domain.entity.Topic
import adeldolgov.tinkoff.asynclecture.domain.repository.TopicRepository
import androidx.annotation.WorkerThread
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

/**
 * @author ad.dolgov
 */
internal class TopicRepositoryImpl : TopicRepository {

    override fun loadTopics(): Observable<List<Topic>> {
        return Observable.fromCallable { generateTopicList() }
            .delay(1000L, TimeUnit.MILLISECONDS)
    }

    @WorkerThread
    private fun generateTopicList(): List<Topic> {
        return listOf(
            Topic(
                "1",
                "Заголовок",
                "Долгов Адель",
                "28.10.2021 : 20:21",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus enim eget mauris dictum tincidunt. Etiam ipsum massa, tincidunt sed nisl nec, imperdiet consectetur urna. In finibus congue felis, ut condimentum erat feugiat sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc bibendum sodales lobortis. Nullam vulputate mauris ac massa porta vulputate"
            ),
            Topic(
                "2",
                "Ржачное видео смотреть всем (2021)",
                "Александр Анохин",
                "28.10.2021 : 20:22",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus enim eget mauris dictum tincidunt. Etiam ipsum massa, tincidunt sed nisl nec, imperdiet consectetur urna. In finibus congue felis, ut condimentum erat feugiat sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc bibendum sodales lobortis. Nullam vulputate mauris ac massa porta vulputate"
            ),
            Topic(
                "3",
                "Банк России выпустил монету с местом под рекламу",
                "Сергеев Валентин",
                "28.10.2021 : 20:22",
                "На лицевой стороне монет расположено рельефное изображение Государственного герба Российской Федерации, над ним вдоль канта — надпись полукругом: «РОССИЙСКАЯ ФЕДЕРАЦИЯ», обрамленная с обеих сторон сдвоенными ромбами, справа — товарный знак Московского монетного двора, внизу под гербом в центре в три строки — надпись: «БАНК РОССИИ», номинал монет: «25 РУБЛЕЙ», дата: «2021 г»."
            ),
            Topic(
                "4",
                "Болгария оформила патентное право на болгарку",
                "Сергеев Валентин",
                "28.10.2021 : 20:22",
                "«Ежегодно в России и других постсоветских странах покупают несколько миллионов шлифовальных машин под знаменитым брендом «болгарка». Зная о высоком качестве товаров, производимых в нашей стране, реализаторы зачастую обманывают покупателей, подсовывая им инструменты более низкого качества, да ещё и не производят отчисления Болгарии. От их непорядочных действий страна ежегодно теряет от 5 до 10 млрд долларов. Получение регистрации в международном патентном бюро позволит нам в судебном порядке взыскать средства с недобросовестных спекулянтов и восстановить и справедливость», - заявил директор Национального агентства Болгарии по авторскому и патентному праву Христо Двораков."
            ),
            Topic(
                "5",
                "В «Сколково» вывели собаку, которая самостоятельно прошла курсы фронтенд-разработки в «Яндексе»",
                "Сергеев Валентин",
                "28.10.2021 : 20:24",
                "Проект был реализован сотрудниками лаборатории генной инженерии и биотехнологий «Сколково». В результате генетического эксперимента была выведена совершенно новая порода собаки, которую назвали ризенбраузер. По словам создателей, ризенбраузер отличается высокой обучаемостью, нетривиальным мышлением и врожденным знанием веб-стандартов."
            ),
            Topic(
                "5",
                "Ясос Биба, биография",
                "Александр Анохин",
                "28.10.2021 : 20:25",
                "Биография Ясоса Бибы"
            )
        )
    }
}
