package adeldolgov.tinkoff.asynclecture.domain.repository

import adeldolgov.tinkoff.asynclecture.domain.entity.Topic
import io.reactivex.Observable

/**
 * @author ad.dolgov
 */
interface TopicRepository {

    fun loadTopics(): Observable<List<Topic>>
}
