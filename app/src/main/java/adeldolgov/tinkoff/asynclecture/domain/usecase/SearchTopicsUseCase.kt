package adeldolgov.tinkoff.asynclecture.domain.usecase

import adeldolgov.tinkoff.asynclecture.data.TopicRepositoryImpl
import adeldolgov.tinkoff.asynclecture.domain.entity.Topic
import adeldolgov.tinkoff.asynclecture.domain.repository.TopicRepository
import io.reactivex.Observable

/**
 * @author ad.dolgov
 */
interface SearchTopicsUseCase : (String) -> Observable<List<Topic>> {

    override fun invoke(searchQuery: String): Observable<List<Topic>>
}

internal class SearchTopicsUseCaseImpl : SearchTopicsUseCase {

    private val topicRepository: TopicRepository = TopicRepositoryImpl()

    override fun invoke(searchQuery: String): Observable<List<Topic>> {
        return topicRepository.loadTopics()
            .map { topics ->
                topics.filter {
                    it.description.contains(searchQuery, ignoreCase = true) || it.title.contains(searchQuery, ignoreCase = true)
                }
            }
    }

}
