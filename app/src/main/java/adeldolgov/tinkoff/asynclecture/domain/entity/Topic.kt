package adeldolgov.tinkoff.asynclecture.domain.entity

/**
 * @author ad.dolgov
 */
class Topic(
    val id: String,
    val title: String,
    val author: String,
    val time: String,
    val description: String
)
