package adeldolgov.tinkoff.asynclecture.presentation.adapter

import adeldolgov.tinkoff.asynclecture.databinding.ItemTopicBinding
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.Color
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

/**
 * @author ad.dolgov
 */
internal class TopicAdapter :
    ListAdapter<TopicItem, TopicAdapter.TopicViewHolder>(InterestingItemDiffUtilCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicViewHolder {
        return TopicViewHolder(
            ItemTopicBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: TopicViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onViewAttachedToWindow(holder: TopicViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.startAnimation()
    }

    override fun onViewDetachedFromWindow(holder: TopicViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.cancelAnimation()
    }

    class TopicViewHolder(
        private val viewBinding: ItemTopicBinding
    ) : RecyclerView.ViewHolder(viewBinding.root) {

        private val rgbAnimation: ValueAnimator = ObjectAnimator.ofArgb(Color.RED, Color.GREEN, Color.BLUE).apply {
            addUpdateListener {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    viewBinding.root.outlineAmbientShadowColor = it.animatedValue as Int
                    viewBinding.root.outlineSpotShadowColor = it.animatedValue as Int
                }
            }
            duration = ANIMATION_DURATION_RGB_MILLIS
            repeatCount = ValueAnimator.INFINITE
            repeatMode = ValueAnimator.REVERSE
        }

        fun startAnimation() = rgbAnimation.start()

        fun cancelAnimation() = rgbAnimation.cancel()

        fun bind(topicItem: TopicItem) {
            viewBinding.descriptionText.text = topicItem.description
            viewBinding.titleText.text = topicItem.title
            viewBinding.titleText.setTextColor(ContextCompat.getColor(viewBinding.root.context, topicItem.textColorRes))
            viewBinding.signumText.text = topicItem.time.plus(" (c) ${topicItem.author}")
        }

        companion object {

            private const val ANIMATION_DURATION_RGB_MILLIS = 2300L
        }
    }

    class InterestingItemDiffUtilCallback : DiffUtil.ItemCallback<TopicItem>() {

        override fun areItemsTheSame(oldItem: TopicItem, newItem: TopicItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: TopicItem, newItem: TopicItem): Boolean {
            return oldItem == newItem
        }

    }
}
