package adeldolgov.tinkoff.asynclecture.presentation

import adeldolgov.tinkoff.asynclecture.domain.usecase.SearchTopicsUseCase
import adeldolgov.tinkoff.asynclecture.domain.usecase.SearchTopicsUseCaseImpl
import adeldolgov.tinkoff.asynclecture.presentation.adapter.TopicToItemMapper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 * @author ad.dolgov
 */
internal class MainViewModel : ViewModel() {

    private var _mainScreenState: MutableLiveData<MainScreenState> = MutableLiveData()
    val mainScreenState: LiveData<MainScreenState>
        get() = _mainScreenState

    private val searchTopicsUseCase: SearchTopicsUseCase = SearchTopicsUseCaseImpl()
    private val topicToItemMapper: TopicToItemMapper = TopicToItemMapper()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val searchSubject: PublishSubject<String> = PublishSubject.create()

    init {
        subscribeToSearchChanges()
    }

    fun searchTopics(searchQuery: String) {
        searchSubject.onNext(searchQuery)
    }

    private fun subscribeToSearchChanges() {
        searchSubject
            .subscribeOn(Schedulers.io())
            .distinctUntilChanged()
            .doOnNext { _mainScreenState.postValue(MainScreenState.Loading) }
            .debounce(500, TimeUnit.MILLISECONDS, Schedulers.io())
            .switchMap { searchQuery -> searchTopicsUseCase(searchQuery) }
            .map(topicToItemMapper)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { _mainScreenState.value = MainScreenState.Result(it) },
                onError = { _mainScreenState.value = MainScreenState.Error(it) }
            )
            .addTo(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    companion object {

        const val INITIAL_QUERY: String = ""
    }
}
