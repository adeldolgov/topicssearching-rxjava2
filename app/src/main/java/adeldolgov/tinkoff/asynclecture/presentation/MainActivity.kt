package adeldolgov.tinkoff.asynclecture.presentation

import adeldolgov.tinkoff.asynclecture.databinding.ActivityMainBinding
import adeldolgov.tinkoff.asynclecture.presentation.adapter.TopicAdapter
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged

internal class MainActivity : AppCompatActivity() {

    private val viewBinding: ActivityMainBinding by lazy(LazyThreadSafetyMode.NONE) {
        ActivityMainBinding.inflate(layoutInflater)
    }
    private val viewModel: MainViewModel by viewModels()
    private val topicAdapter = TopicAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        initUi()
        if (savedInstanceState == null) viewModel.searchTopics(MainViewModel.INITIAL_QUERY)
    }

    private fun initUi() {
        viewBinding.recyclerView.adapter = topicAdapter
        viewModel.mainScreenState.observe(this) { processMainScreenState(it) }
        viewBinding.searchEditText.doAfterTextChanged {
            viewModel.searchTopics(it.toString())
        }
    }

    private fun processMainScreenState(it: MainScreenState?) {
        when (it) {
            is MainScreenState.Result -> {
                topicAdapter.submitList(it.items) { viewBinding.recyclerView.scrollToPosition(0) }
                viewBinding.loadingProgress.isVisible = false
            }
            MainScreenState.Loading -> {
                viewBinding.loadingProgress.isVisible = true
            }
            is MainScreenState.Error -> {
                Toast.makeText(this, it.error.message, Toast.LENGTH_SHORT).show()
                viewBinding.loadingProgress.isVisible = false
            }
        }
    }
}
