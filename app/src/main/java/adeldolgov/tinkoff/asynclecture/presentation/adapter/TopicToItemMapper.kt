package adeldolgov.tinkoff.asynclecture.presentation.adapter

import adeldolgov.tinkoff.asynclecture.R
import adeldolgov.tinkoff.asynclecture.domain.entity.Topic

/**
 * @author ad.dolgov
 */
internal class TopicToItemMapper : (List<Topic>) -> (List<TopicItem>) {

    override fun invoke(topics: List<Topic>): List<TopicItem> {
        return topics.map { topic ->
            TopicItem(
                id = topic.id,
                title = topic.title,
                author = topic.author,
                time = topic.time,
                description = topic.description,
                textColorRes = if (topic.title.length % 2 == 0) R.color.white else R.color.purple_200
            )
        }
    }
}
