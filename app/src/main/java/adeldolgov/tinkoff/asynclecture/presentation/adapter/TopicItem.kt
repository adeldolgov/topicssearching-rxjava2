package adeldolgov.tinkoff.asynclecture.presentation.adapter

import androidx.annotation.ColorRes

/**
 * @author ad.dolgov
 */
internal data class TopicItem(
    val id: String,
    val title: String,
    val author: String,
    val time: String,
    val description: String,
    @ColorRes
    val textColorRes: Int,
)
