package adeldolgov.tinkoff.asynclecture.presentation

import adeldolgov.tinkoff.asynclecture.presentation.adapter.TopicItem

/**
 * @author ad.dolgov
 */
internal sealed class MainScreenState {

    class Result(val items: List<TopicItem>) : MainScreenState()

    object Loading : MainScreenState()

    class Error(val error: Throwable) : MainScreenState()
}
